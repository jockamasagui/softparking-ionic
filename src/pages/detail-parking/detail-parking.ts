import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
/**
 * Generated class for the DetailParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-parking',
  templateUrl: 'detail-parking.html',
})
export class DetailParkingPage {

  public title: string;
  public parking: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _launchNavigator: LaunchNavigator, public geolocation: Geolocation,
    public diagnostic: Diagnostic
  ) {
    this.parking = navParams.get('parking');
  }

  ionViewDidLoad() { }

  showMap() {
    let coor = {
      lat: this.parking.lat,
      lng: this.parking.lng
    }

    this.diagnostic.isGpsLocationEnabled().then((rest) => {
      if (rest) {
        this.geolocation.getCurrentPosition().then((res: Geoposition) => {
          let lat = res.coords.latitude;
          let lng = res.coords.longitude;

          this._launchNavigator.isAppAvailable(this._launchNavigator.APP.GOOGLE_MAPS).then((res) => {
            if (res) {
              this._launchNavigator.navigate([lat, lng], {
                start: coor.lat + " , " + coor.lng
              });
            } else {
              alert("Es necesario instalar google maps");
            }
          }).catch((err) => {
            alert(JSON.stringify(err));
          })
        }).catch((err) => {
          alert(JSON.stringify(err));
        })
      } else {
        alert('Por favor encender su gps');
        this.diagnostic.switchToLocationSettings();
      }
    });
  }
  /*
        this._launchNavigator.isAppAvailable(this._launchNavigator.APP.GOOGLE_MAPS).then((res) => {
        if (res) {
          this._launchNavigator.navigate([lat,lng], {
            start: coor.lat+" , "+coor.lng
          });
        } else {
          alert("Google Maps not available - falling back to user selection");
        }
      }).catch((err) => {
        alert(JSON.stringify(err));
      })
  */

}
