import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailParkingPage } from './detail-parking';

@NgModule({
  declarations: [
    DetailParkingPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailParkingPage),
  ],
})
export class DetailParkingPageModule {}
