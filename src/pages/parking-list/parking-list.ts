import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ParkingProvider  } from '../../providers/parking/parking';
/**
 * Generated class for the ParkingListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parking-list',
  templateUrl: 'parking-list.html',
})
export class ParkingListPage {

  public listParkings;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public _parkingProvider:ParkingProvider,public _loadingController:LoadingController) {
  }

  ionViewDidLoad() {
    this.loadAllParkings();
  }

  loadAllParkings(){
       let loadingCtrl = this._loadingController.create({
      content:'Cargando'
    });
     loadingCtrl.present();
    this._parkingProvider.getAllParking().subscribe((rest:any) =>{
      this.listParkings = rest.lista_p;
      
    },err =>{
      loadingCtrl.dismiss();
     alert('Error en la petición');
    },()=>{
      loadingCtrl.dismiss();
    });
  }

  detailParking(parking){
    this.navCtrl.push('DetailParkingPage',{parking:parking});
  }

  updateList(refresher){
    this.loadAllParkings();
    refresher.complete();
  }

}
